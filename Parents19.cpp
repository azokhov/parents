// Parents19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>


class Animal
{
private:
	std::string animalVoice;
	std::string animalName;
public:
	//Animal() {}

	virtual void Voice() = 0;
	virtual void Name() = 0;
};


class Dog : public Animal
{
private:
	std::string animalVoice = "Woof!\n";
	std::string animalName = "Dog goes:\t";
public:
	void Voice() override
	{
		std::cout << animalVoice;
	}
	void Name() override
	{
		std::cout << animalName;
	}
};

class Cat : public Animal
{
private:
	std::string animalVoice = "Meow!\n";
	std::string animalName = "Cat goes:\t";
public:
	void Voice() override
	{
		std::cout << animalVoice;
	}
	void Name() override
	{
		std::cout << animalName;
	}
};

class Bird : public Animal
{
private:
	std::string animalVoice = "Tweet!\n";
	std::string animalName = "Bird goes:\t";
public:
	void Voice() override
	{
		std::cout << animalVoice;
	}
	void Name() override
	{
		std::cout << animalName;
	}
};


int main()
{
	const int size = 3;
	Animal *arrAnimal[size] = { new Dog, new Cat, new Bird}; 

	

	//Animal* p1 = new Dog();
	//Animal* p2 = new Cat();
	//Animal* p3 = new Bird();

	//*arrAnimal[0] = *p1;
	//*arrAnimal[1] = *p2;
	//*arrAnimal[2] = *p3;



	for (int i = 0; i < size; i++)
	{
		arrAnimal[i]->Name();
		arrAnimal[i]->Voice();
	}
	//p1->Voice();
	//p2->Voice();
	//p3->Voice();
	
}